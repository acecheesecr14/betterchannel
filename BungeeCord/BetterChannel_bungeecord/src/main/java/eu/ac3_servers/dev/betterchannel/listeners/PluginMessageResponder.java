package eu.ac3_servers.dev.betterchannel.listeners;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Date;

import eu.ac3_servers.dev.betterchannel.BetterChannel;
import eu.ac3_servers.dev.betterchannel.events.MessageEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PluginMessageResponder implements Listener {
	
	private BetterChannel plugin;

	public PluginMessageResponder(BetterChannel plugin) {
		this.plugin = plugin;
		this.plugin.getProxy().getPluginManager().registerListener(plugin, this);
		this.plugin.debug("Registered listener: " + getClass().getName());
	}
	
	@EventHandler
	public void onPluginMessage(PluginMessageEvent e){
		MessageEvent event = new MessageEvent(e.getData(), e.getReceiver(), e.getSender());
		this.plugin.getProxy().getPluginManager().callEvent(event);
		this.plugin.debug("Created a new MessageEvent @ " + new Date(event.getTimeCreated()).toString());
	}
	
	@SuppressWarnings("static-access")
	@EventHandler
	public void onMessageEvent(MessageEvent e){
		DataInputStream data = e.getData();
		try {
			String command = data.readUTF();
			if(command.equalsIgnoreCase("KICK")){
				String names = data.readUTF();
				String KickReason = data.readUTF();
				String[] namea = names.split(" ");
				for (String string : namea) {
					ProxiedPlayer player = this.plugin.getProxy().getPlayer(string);
					if(player != null){
						player.disconnect(new TextComponent(KickReason));
						this.plugin.debug("Kicked " + string + " for " + KickReason);
					}
				}
			}
		} catch (IOException e1) {
			if(plugin.debugging()) e1.printStackTrace();
			plugin.getLogger().warning("Incorrect formatting for a plugin message?");
		}
	}
}
