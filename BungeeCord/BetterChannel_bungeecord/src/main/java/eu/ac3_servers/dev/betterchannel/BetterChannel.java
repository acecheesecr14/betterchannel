package eu.ac3_servers.dev.betterchannel;

import net.md_5.bungee.api.plugin.Plugin;

public class BetterChannel extends Plugin {

	private BetterChannelConfig config;
	private static BetterChannel instance;
	private static boolean debug;
	public final static String ChannelName = "BungeeCord";

	@Override
	public void onEnable() {
		this.config = new BetterChannelConfig(this);
		debug = this.config.getConfig().getBoolean("debugging");
		debug("Registered config.");
		
		getProxy().registerChannel(ChannelName);
		debug("Registered channel", ChannelName);
		
		instance = this;
		debug("Initialized.");
	}
	
	public static boolean debugging(){
		return debug;
	}
	
	public static BetterChannel getBetterChannel(){
		return instance;
	}
	
	public void debug(String... text){
		for (String string : text) {
			getLogger().info(new StringBuilder("[D] ").append(string).toString());
		}
		return;
	}
	
}
