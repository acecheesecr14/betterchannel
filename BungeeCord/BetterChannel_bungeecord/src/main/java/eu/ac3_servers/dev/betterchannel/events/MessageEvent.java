package eu.ac3_servers.dev.betterchannel.events;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.plugin.Event;

public class MessageEvent extends Event {
	
	private byte[] dataRaw;
	private Connection Receiver;
	private Connection Sender;
	private DataInputStream data;
	private long timeCreated;

	/**
	 * Create a new event for the plugin message to be parsed.
	 * 
	 * @param data
	 * @param Receiver
	 * @param Sender
	 */
	public MessageEvent(byte[] data, Connection Receiver, Connection Sender) {
		this.timeCreated = System.currentTimeMillis();
		this.dataRaw = data;
		this.Receiver = Receiver;
		this.Sender = Sender;
		this.data = new DataInputStream(new ByteArrayInputStream(this.dataRaw));
	}
	
	/**
	 * Returns the data as a byte array.
	 * @return byte[] data
	 */
	public byte[] getDataRaw(){
		return this.dataRaw;
	}
	
	/**
	 * Returns the easy to read and clean DataInputStream.
	 * @return DataInputStream inpit
	 */
	public DataInputStream getData(){
		return this.data;
	}
	
	/**
	 * Return the receiver of the plugin message.
	 * This is the PLAYER of player.sendPluginMessage(...);
	 * @return Connection receiver.
	 */
	public Connection getReceiver(){
		return this.Receiver;
	}
	
	/**
	 * Return the sender of the plugin message.
	 * @return Connection sender
	 */
	public Connection getSender(){
		return this.Sender;
	}
	
	/**
	 * Get the time the event was created.
	 * @return Long time
	 */
	public long getTimeCreated(){
		return this.timeCreated;
	}

}
